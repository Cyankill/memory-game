'use strict';
// Card options
const cardArray = [
  { name: 'fries', img: 'images/fries.png' },
  { name: 'cheeseburger', img: 'images/cheeseburger.png' },
  { name: 'ice-cream', img: 'images/ice-cream.png' },
  { name: 'pizza', img: 'images/pizza.png' },
  { name: 'milkshake', img: 'images/milkshake.png' },
  { name: 'hotdog', img: 'images/hotdog.png' },
  { name: 'fries', img: 'images/fries.png' },
  { name: 'cheeseburger', img: 'images/cheeseburger.png' },
  { name: 'ice-cream', img: 'images/ice-cream.png' },
  { name: 'pizza', img: 'images/pizza.png' },
  { name: 'milkshake', img: 'images/milkshake.png' },
  { name: 'hotdog', img: 'images/hotdog.png' },
];

cardArray.sort(() => 0.5 - Math.random());

const grid = document.querySelector('.grid');
const resultDisplay = document.querySelector('#result');
let cardsChosen = [];
let cardsChosenId = [];
const cardsWon = [];

// Board creation
function createBoard() {
  for (let i = 0; i < cardArray.length; i++) {
    const card = document.createElement('img');
    card.setAttribute('src', 'images/blank.png');
    card.setAttribute('data-id', i);
    card.addEventListener('click', flipCard);
    grid.appendChild(card);
  }
}

// Check for matches
function checkMatch() {
  const cards = document.querySelectorAll('.grid img');
  const option0Id = cardsChosenId[0];
  const option1Id = cardsChosenId[1];
  console.log('check for match!');
  if (option0Id == option1Id) {
    cards[option0Id].setAttribute('src', 'images/blank.png');
    cards[option1Id].setAttribute('src', 'images/blank.png');
    alert('you have clicked the same images!');
  } else if (cardsChosen[0] === cardsChosen[1]) {
    alert('You found a match!');
    cards[option0Id].setAttribute('src', 'images/white.png');
    cards[option1Id].setAttribute('src', 'images/white.png');
    cards[option0Id].removeEventListener('click', flipCard);
    cards[option1Id].removeEventListener('click', flipCard);
    cardsWon.push(cardsChosen);
  } else {
    cards[option0Id].setAttribute('src', 'images/blank.png');
    cards[option1Id].setAttribute('src', 'images/blank.png');
    alert('Sorry try again');
  }
  resultDisplay.textContent = cardsWon.length;
  cardsChosen = [];
  cardsChosenId = [];
  if (cardsWon.length === cardArray.length / 2) {
    resultDisplay.textContent = 'Congratulations you found them all';
  }
}

// Flip the card
function flipCard() {
  const cardId = this.getAttribute('data-id');
  cardsChosen.push(cardArray[cardId].name);
  cardsChosenId.push(cardId);
  this.setAttribute('src', cardArray[cardId].img);
  if (cardsChosen.length === 2) {
    setTimeout(checkMatch, 500);
  }
}

createBoard();
